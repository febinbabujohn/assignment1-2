import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BankTest {
	// When a new Bank is created, checking if it has 0 customers
	@Test
	public void testInitialNumberOfCustomers() {

		Bank b = new Bank();
		int expectedNum = 0;
		int actualNum = b.getNumberOfCustomers();
		assertEquals(expectedNum, actualNum);
	}

	// Adding customers to the bank and checking if the number of customer increased
	// by 1
	@Test
	public void testAdditionOfCustomers() {
		Bank b = new Bank();
		int num = b.getNumberOfCustomers();
		int expectedNum = num + 1;
		b.addCustomer("kiran", 100);
		int actualnum = b.getNumberOfCustomers();
		assertEquals(expectedNum, actualnum);
	}

	// Remove a customer and check if the count of customer is decreased by 1
	@Test
	public void testRemovalOfCustomers() {
		Bank b = new Bank();
		b.addCustomer("kiran", 100);
		int num = b.getNumberOfCustomers();
		int expectedNum = num - 1;
		b.removeCustomer("kiran");
		int actualnum = b.getNumberOfCustomers();
		assertEquals(expectedNum, actualnum);
	}

	// Checking if account transfer is possible through customer name

	@Test
	public void testTransferAmount() {
		Bank b = new Bank();
		b.addCustomer("kiran", 100);
		b.addCustomer("febin", 1);
		Customer A = b.getCustomers().get(0);
		Customer B = b.getCustomers().get(1);
		int transferAmt = 50;
		int expecAbal = A.getAccount().balance() - transferAmt;
		int expecBbal = (B.getAccount().balance()) + transferAmt;
		b.transferMoney(A.getName(), B.getName(), transferAmt);
		// This code will work once we pass objects as arguments to the fuction
		// transferMoney(b.transferMoney(A, B, transferAmt))
		int actualAbal = A.getAccount().balance();
		int actualBbal = B.getAccount().balance();
		assertEquals(expecAbal, actualAbal);
		assertEquals(expecBbal, actualBbal);

	}
}

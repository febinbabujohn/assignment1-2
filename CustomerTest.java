import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class CustomerTest {
	// Checking if the account is created for the newly created customer
	@Test
	public void testCustomerAccount() {
		Customer a = new Customer("kiran", 100);
		assertNotNull(a.getAccount());

	}

	// Checking account balance for the customer with initial deposit amount passed
	@Test
	public void testCustomerAccountBalancewithInitailValue() {
		Customer a = new Customer("kiran", 100);
		int expectedBal = 100;
		int actualBal = a.getAccount().balance();
		assertEquals(expectedBal, actualBal);

	}

	// Checking account balance for the customer without passing initial deposit
	// amount
	@Test
	public void testCustomerAccountBalancewithoutInitailValue() {
		Customer a = new Customer("kiran", 0);
		int expectedBal = 0;
		int actualBal = a.getAccount().balance();
		assertEquals(expectedBal, actualBal);

	}

}

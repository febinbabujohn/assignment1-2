import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AccountTest {

	// : All new accounts are initialized with 0 dollars
	@Test
	public void testAllAccountsInitializeWithZero() {
		Account acc = new Account(100);
		int actualBal = acc.balance();
		int expectedBal = 0;
		assertEquals(expectedBal, actualBal);
	}

	// : Deposit money to an account
	@Test
	public void testdeposit() {

		Account acc = new Account(1500);
		int expectedbal = acc.balance() + 1000;
		acc.deposit(1000);
		int actualbal = acc.balance();
		assertEquals(expectedbal, actualbal);

	}

	// Withdraw money from an account
	@Test
	public void testwithdraw() {
		Account acc = new Account(2500);
		int expectedbal = acc.balance() - 500;
		acc.withdraw(500);
		int actualbal = acc.balance();
		assertEquals(expectedbal, actualbal);
	}

	// Withdraw excessive amount from the account
	@Test
	public void testOverDraft() {
		Account acc = new Account(2500);
		int expectedbal = acc.balance();
		acc.withdraw(3000);
		int actualbal = acc.balance();
		assertEquals(expectedbal, actualbal);
	}

	// : Trying to deposit negative amount to the account
	@Test
	public void testInvalidArgument() {

		Account acc = new Account(1500);
		int expectedbal = acc.balance();
		acc.deposit(-1000);
		int actualbal = acc.balance();
		assertEquals(expectedbal, actualbal);

	}

}
